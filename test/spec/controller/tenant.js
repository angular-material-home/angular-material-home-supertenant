'use strict';

describe('AmhTenantCtrl ', function() {

	// load the controller's module
	beforeEach(module('ngMaterialHomeSuperTenant'));

	var AmhTenantNewCtrl;
	var scope;
	var $rootScope;
	var $controller;

	// Initialize the controller and a mock scope
	beforeEach(inject(function(_$controller_, _$rootScope_) {
		$rootScope = _$rootScope_;
		$controller = _$controller_;
	}));

	it('should exist controller', function() {
		scope = $rootScope.$new();
		AmhTenantNewCtrl = $controller('AmhTenantCtrl', {
			$scope : scope,
			// place here mocked dependencies
		});
		 expect(AmhTenantNewCtrl).not.toBe(null);
	});
	
	it('should attach a reload function to the scope', function() {
		scope = $rootScope.$new();
		AmhTenantNewCtrl = $controller('AmhTenantCtrl', {
			$scope : scope,
			// place here mocked dependencies
		});
		expect(angular.isFunction(scope.reload)).toBe(true);
	});
	
	it('should attach a update function to the scope', function() {
		scope = $rootScope.$new();
		AmhTenantNewCtrl = $controller('AmhTenantCtrl', {
			$scope : scope,
			// place here mocked dependencies
		});
		expect(angular.isFunction(scope.update)).toBe(true);
	});
	
	it('should attach a remove function to the scope', function() {
		scope = $rootScope.$new();
		AmhTenantNewCtrl = $controller('AmhTenantCtrl', {
			$scope : scope,
			// place here mocked dependencies
		});
		expect(angular.isFunction(scope.remove)).toBe(true);
	});
});
