# AMH Super Tenant

[![pipeline status](https://gitlab.com/angular-material-home/angular-material-home-supertenant/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/angular-material-home-supertenant/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9d634abd45c643369ae455fc3b1d2a64)](https://www.codacy.com/app/angular-material-home/angular-material-home-supertenant?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/angular-material-home-supertenant&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/9d634abd45c643369ae455fc3b1d2a64)](https://www.codacy.com/app/angular-material-home/angular-material-home-supertenant?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/angular-material-home-supertenant&utm_campaign=Badge_Coverage)

Provides tools to manage and explore tenants.


## Install

This is an bower package and you can install view the following command.

	bower install --save angular-material-home-supertenant

It is recommanded to use a full path of the Git as follow:

	bower install --save https://gitlab.com/angular-material-home/angular-material-home-supertenant.git

## Document

- [Wiki](https://gitlab.com/angular-material-home/angular-material-home-supertenant/wikis/home)
- [API document](https://angular-material-home.gitlab.io/angular-material-home-supertenant/doc/index.html)