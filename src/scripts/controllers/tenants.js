/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSuperTenant')


/**
 * @ngdoc controller
 * @name AmhTenantsCtrl
 * @description List all tenants
 * 
 * Manages views of all tenants
 * 
 * @property {Object} ctrl -Controller options
 * @property {Object} ctrl.error -Last internal error
 * @property {boolean} ctrl.loadingTenants -True if ctrl working on tenants list
 * @peoperty {Array<Tenant>} ctrl.items -List of loaded tenants
 * @property {Array<Action>} ctrl.actions -List of possible action from View
 * @property {Array<string>} ctrl.sortKeys -List of keys which are used to sort the list
 * @property {Array<string>} ctrl.filterKeys -List of keys which are used to filter list itmes.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
.controller('AmhTenantsCtrl', function ($scope, $supertenant, $sce, $q, $navigator, $controller) {

	angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	}));

	// Overried the function
	this.getSchema = function () {
		return $supertenant.tenantSchema();
	};

	// get accounts
	this.getModels = function (parameterQuery) {
		return $supertenant.getTenants(parameterQuery);
	};

	this.secureUrl = function (url) {
		var secureUrl = 'http://' + url;
		return $sce.trustAsResourceUrl(secureUrl);
	};

	this.openSite = function (url) {
		$navigator.openPage('http://' + url);
	};

	this.search = function (searchKey) {
		this.queryParameter.setQuery(searchKey);
		this.reload();
	};

	this.queryParameter.removeSorter('id');
	this.queryParameter.setOrder('level', 'd');
	this.init({
		eventType: '/tenant/tenants'
	});
});
