/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSuperTenant')


/**
 * @ngdoc controller
 * @name AmhTenantNewCtrl
 * @description Create new tenant
 * 
 * Manages views to create a new tenant.
 * 
 * - Can be used with a WB widget
 * - Can be used with ngRout ($routeParme)
 * 
 * @property {Object} ctrl - Manage controller options
 * @property {boolean} ctrl.autoRedirect - Redirect to the new tenant automatically
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
.controller('AmhTenantNewCtrl', function ($scope, $location, $navigator, $supertenant, $rootScope) {

	var ctrl = this;
	ctrl.state = 'ready'; // state of the ctrl
	ctrl.tenant = null; // Current tenant
	ctrl.wbModel = $scope.wbModel; // corent config

	var wbModel = $scope.wbModel;
	var stateMachine;

	// TODO: maso, 2018: remove if bug fixed:
	// https://gitlab.com/weburger/angular-material-weburger/issues/57
	if (!$scope.app) {
		$scope.app = $rootScope.app;
	} else {
		console.log('This part is not required any more')
	}

	/**
	 * Creates new tenant
	 * 
	 * @memberof AmhTenantNewCtrl
	 * @param {Object} data - Form to create tenant
	 * @return {promise<Tenant>} to create a new tenant
	 */
	function crateTenant(data) {
		return $supertenant.putTenant(data)//
		.then(function (tenant) {
			stateMachine.tenantIsCreated(tenant);
		},function(error){
			stateMachine.error(error);
		});
	}

	/*
	 * Manages the state of the controller
	 */
	stateMachine = new machina.Fsm({
		namespace: 'amh.supertenant.new',
		initialState: 'ready',
		states: {
			ready: {
				createNewTenant: 'loading'
			},
			loading: {
				created: 'info',
				error4XX: 'E4XX',
				error5XX: 'E5XX'
			},
			info: {},
			E4XX: {
				createNewTenant: 'loading'
			},
			E5XX: {},
		},

		/*
		 * Meta date of the content id loaded
		 */
		createNewTenant: function (data) {
			crateTenant(data);
			this.handle('createNewTenant');
		},
		tenantIsCreated: function (tenant){
			ctrl.tenant = tenant;
			if (wbModel.autoRedirect) {
				$navigator.openPage('http://' + tenant.domain + '/' +  (wbModel.landingPage || '/'));
			}
			this.handle('created');
		},
		error: function (error) {
			ctrl.error = error.data;
			switch (error.status) {
			case 400:
			case 404:
			case 401:
			case 403:
				this.handle('error4XX');
				break;
			case 500:
				this.handle('error5XX');
				break;
			}
		}
	});


	/*
	 * Lesson to the state machine transitions
	 */
	stateMachine.on('transition', function () {
		ctrl.state = stateMachine.state;
	});
	ctrl.crateTenant = function(tenantData){
		stateMachine.createNewTenant(tenantData);
	};
	
	$scope.newTenant = ctrl.crateTenant;
});
