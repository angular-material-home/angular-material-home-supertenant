/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeSuperTenant')
.run(function($widget) {

	/**
	 * @ngdoc Widgets
	 * @name amh-super-tenant-new
	 * @description A widget to create a tenant.
	 */
	$widget.newWidget({
		type : 'amh-super-tenant-new',
		title: 'site register',
		label : 'Submit tenant',
		description : 'A widget to submit a new site.',
		icon : 'home',
		
		controller: 'AmhTenantNewCtrl',
		controllerAs: 'widgetCtrl',
		templateUrl : 'views/widgets/amh-tenant-new.html',
		
		help : 'http://dpq.co.ir/more-information-link',
		setting : [ 'tenant-landing'],
	});
	
	/**
	 * @ngdoc wb-widget
	 * @name amh-super-tenant-new
	 * @description A widget to create a tenant.
	 */
	$widget.newWidget({
		type : 'amh-super-tenant-tenants',
		templateUrl : 'views/widgets/tenants.html',
                title: 'Tenants',
		label : 'Tenants',
		description : 'A widget to show tenants.',
		icon : 'home',
		help : '',
		setting : ''
	});
});