/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * 
 */
angular.module('ngMaterialHomeSuperTenant', [
	'seen-supertenant',
	'ngMaterialHome'
]);
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSuperTenant')


/**
 * @ngdoc controller
 * @name AmhTenantNewCtrl
 * @description Create new tenant
 * 
 * Manages views to create a new tenant.
 * 
 * - Can be used with a WB widget
 * - Can be used with ngRout ($routeParme)
 * 
 * @property {Object} ctrl - Manage controller options
 * @property {boolean} ctrl.autoRedirect - Redirect to the new tenant automatically
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
.controller('AmhTenantNewCtrl', function ($scope, $location, $navigator, $supertenant, $rootScope) {

	var ctrl = this;
	ctrl.state = 'ready'; // state of the ctrl
	ctrl.tenant = null; // Current tenant
	ctrl.wbModel = $scope.wbModel; // corent config

	var wbModel = $scope.wbModel;
	var stateMachine;

	// TODO: maso, 2018: remove if bug fixed:
	// https://gitlab.com/weburger/angular-material-weburger/issues/57
	if (!$scope.app) {
		$scope.app = $rootScope.app;
	} else {
		console.log('This part is not required any more')
	}

	/**
	 * Creates new tenant
	 * 
	 * @memberof AmhTenantNewCtrl
	 * @param {Object} data - Form to create tenant
	 * @return {promise<Tenant>} to create a new tenant
	 */
	function crateTenant(data) {
		return $supertenant.putTenant(data)//
		.then(function (tenant) {
			stateMachine.tenantIsCreated(tenant);
		},function(error){
			stateMachine.error(error);
		});
	}

	/*
	 * Manages the state of the controller
	 */
	stateMachine = new machina.Fsm({
		namespace: 'amh.supertenant.new',
		initialState: 'ready',
		states: {
			ready: {
				createNewTenant: 'loading'
			},
			loading: {
				created: 'info',
				error4XX: 'E4XX',
				error5XX: 'E5XX'
			},
			info: {},
			E4XX: {
				createNewTenant: 'loading'
			},
			E5XX: {},
		},

		/*
		 * Meta date of the content id loaded
		 */
		createNewTenant: function (data) {
			crateTenant(data);
			this.handle('createNewTenant');
		},
		tenantIsCreated: function (tenant){
			ctrl.tenant = tenant;
			if (wbModel.autoRedirect) {
				$navigator.openPage('http://' + tenant.domain + '/' +  (wbModel.landingPage || '/'));
			}
			this.handle('created');
		},
		error: function (error) {
			ctrl.error = error.data;
			switch (error.status) {
			case 400:
			case 404:
			case 401:
			case 403:
				this.handle('error4XX');
				break;
			case 500:
				this.handle('error5XX');
				break;
			}
		}
	});


	/*
	 * Lesson to the state machine transitions
	 */
	stateMachine.on('transition', function () {
		ctrl.state = stateMachine.state;
	});
	ctrl.crateTenant = function(tenantData){
		stateMachine.createNewTenant(tenantData);
	};
	
	$scope.newTenant = ctrl.crateTenant;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSuperTenant')

/**
 * @ngdoc controller
 * @name AmhTenantCtrl
 * @description A tenant controller
 * 
 */
.controller('AmhTenantCtrl', function($scope) {

	$scope.newTenant = function() {
		// TODO: Check test
	};

	$scope.reload = function() {
		// TODO: check unit tests
	};

	$scope.update = function() {
		// TODO: check unit tests
	};
	
	$scope.remove = function(){
		// TODO: check unit tests
	};
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialHomeSuperTenant')


/**
 * @ngdoc controller
 * @name AmhTenantsCtrl
 * @description List all tenants
 * 
 * Manages views of all tenants
 * 
 * @property {Object} ctrl -Controller options
 * @property {Object} ctrl.error -Last internal error
 * @property {boolean} ctrl.loadingTenants -True if ctrl working on tenants list
 * @peoperty {Array<Tenant>} ctrl.items -List of loaded tenants
 * @property {Array<Action>} ctrl.actions -List of possible action from View
 * @property {Array<string>} ctrl.sortKeys -List of keys which are used to sort the list
 * @property {Array<string>} ctrl.filterKeys -List of keys which are used to filter list itmes.
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
.controller('AmhTenantsCtrl', function ($scope, $supertenant, $sce, $q, $navigator, $controller) {

	angular.extend(this, $controller('MbSeenAbstractCollectionCtrl', {
		$scope: $scope
	}));

	// Overried the function
	this.getSchema = function () {
		return $supertenant.tenantSchema();
	};

	// get accounts
	this.getModels = function (parameterQuery) {
		return $supertenant.getTenants(parameterQuery);
	};

	this.secureUrl = function (url) {
		var secureUrl = 'http://' + url;
		return $sce.trustAsResourceUrl(secureUrl);
	};

	this.openSite = function (url) {
		$navigator.openPage('http://' + url);
	};

	this.search = function (searchKey) {
		this.queryParameter.setQuery(searchKey);
		this.reload();
	};

	this.queryParameter.removeSorter('id');
	this.queryParameter.setOrder('level', 'd');
	this.init({
		eventType: '/tenant/tenants'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeSuperTenant')

/*
 * Load settings
 */
.run(function($settings) {
	/**
	 * @ngdoc wb-setting
	 * @name tenant-landing
	 * @description Manages landing page info of a tenant
	 */
	$settings.newPage({
		type: 'tenant-landing',
		label : 'Tenant landing',
		templateUrl : 'views/settings/amh-tenant-landing.html'
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialHomeSuperTenant')
.run(function($widget) {

	/**
	 * @ngdoc Widgets
	 * @name amh-super-tenant-new
	 * @description A widget to create a tenant.
	 */
	$widget.newWidget({
		type : 'amh-super-tenant-new',
		title: 'site register',
		label : 'Submit tenant',
		description : 'A widget to submit a new site.',
		icon : 'home',
		
		controller: 'AmhTenantNewCtrl',
		controllerAs: 'widgetCtrl',
		templateUrl : 'views/widgets/amh-tenant-new.html',
		
		help : 'http://dpq.co.ir/more-information-link',
		setting : [ 'tenant-landing'],
	});
	
	/**
	 * @ngdoc wb-widget
	 * @name amh-super-tenant-new
	 * @description A widget to create a tenant.
	 */
	$widget.newWidget({
		type : 'amh-super-tenant-tenants',
		templateUrl : 'views/widgets/tenants.html',
                title: 'Tenants',
		label : 'Tenants',
		description : 'A widget to show tenants.',
		icon : 'home',
		help : '',
		setting : ''
	});
});
angular.module('ngMaterialHomeSuperTenant').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/settings/amh-tenant-landing.html',
    "<md-list class=wb-setting-panel> <md-input-container> <label translate>Landing page</label> <input ng-model=wbModel.landingPage> </md-input-container> <wb-ui-setting-on-off-switch title=\"Auto Redirect?\" icon=link value=wbModel.autoRedirect> </wb-ui-setting-on-off-switch> </md-list>"
  );


  $templateCache.put('views/widgets/amh-tenant-new.html',
    "<div layout-padding mb-preloading=\"widgetCtrl.state === 'loading'\" flex>  <form ng-submit=widgetCtrl.crateTenant(_tenant) name=newTenantForm layout=column ng-show=\"widgetCtrl.state !== 'info'\"> <md-input-container class=\"md-icon-float md-block\" flex> <label translate>Site name</label> <wb-icon>web</wb-icon> <input name=name ng-model=_tenant.subdomain required minlength=5 maxlength=64 ng-pattern=\"/^[a-zA-Z0-9_\\-]+$/\" aria-label=\"Site name\"> <div ng-messages=newTenantForm.name.$error multiple md-auto-hide=true> <div ng-message=required translate>Tenant name is required</div> <div ng-message=minlength translate>Tenant name must be 5 character at least</div> <div ng-message=maxlength translate>Tenant name must be smaller than 128 character</div> <div ng-message=pattern translate>Tenant name must only contains English letters (a-z or A-Z), digits (0-9), underscore (_) and dash (-)</div> </div> </md-input-container> <md-input-container class=\"md-icon-float md-block\" flex> <label translate>Email</label> <wb-icon>email</wb-icon> <input ng-disable=waiting type=email ng-pattern=\"/\\S+@\\S+\\.\\S+/\" name=name1 ng-model=_tenant.email required aria-label=\"Site owner email\"> <div ng-messages=newTenantForm.email.$error multiple md-auto-hide=true> <div ng-message=required translate>Email is required</div> </div> </md-input-container> <md-input-container class=\"md-icon-float md-block\" flex> <label translate>Phone number</label> <wb-icon>phone</wb-icon> <input ng-pattern=\"/^\\+?\\d+$/\" name=phone ng-model=_tenant.phone required aria-label=\"Tenant owner phone\"> </md-input-container> <md-input-container class=md-block> <md-checkbox name=tos ng-model=tos required aria-label=\"Accept the terms\"> <span translate>I accept the terms of service.</span> </md-checkbox> <div ng-messages=newTenantForm.tos.$error multiple md-auto-hide=false> <div ng-message=required translate>You must accept the terms of service before you can proceed.</div> </div> </md-input-container> <div layout=row layout-align=\"center center\"> <div style=margin-top:30px ng-if=\"app.captcha.engine === 'recaptcha' && app.captcha.recaptcha.key\" vc-recaptcha ng-model=_tenant.g_recaptcha_response theme=\"app.captcha.recaptcha.theme || 'light'\" type=\"app.captcha.recaptcha.type || 'image'\" key=app.captcha.recaptcha.key lang=\"app.setting.local || app.config.local || 'en'\"> </div> </div> <div ng-if=editable layout=row> <span flex></span> <input type=submit hidden> <md-button ng-disabled=!newTenantForm.$valid class=\"md-raised md-primary\"> <span translate>Submit</span> </md-button> </div> <div ng-if=!editable layout=row> <span flex></span> <input type=submit hidden> <md-button ng-click=widgetCtrl.crateTenant(_tenant) ng-disabled=!newTenantForm.$valid class=\"md-raised md-primary\"> <span translate>Submit</span> </md-button> </div> </form>  <div ng-show=\"widgetCtrl.state === 'info'\"> <p translate>Your new tenant is registered. At first, your 'username' and 'password' are 'admin' and 'admin'. Click on 'ok' to go to your site.</p> <div layout=row> <span flex></span> <md-button class=\"md-primary md-raised\" ng-href=\"http://{{widgetCtrl.tenant.domain}}/{{wbModel.landingPage || 'initialization'}}\"> <span trnaslate>Go</span> </md-button> </div> </div> <div layout-padding ng-if=\"widgetCtrl.state === 'E4XX' && widgetCtrl.error.code === 4101\" flex> <h4 style=\"color: red\" translate>Unfortunately, site with the same name is registered before. Please try with another one.</h4> </div> <div layout-padding ng-if=\"widgetCtrl.state === 'E5XX'\" flex> <h4 style=\"color: red\" translate>Unfortunately, some errors occurred in the server. Please try again...</h4> </div> </div>"
  );


  $templateCache.put('views/widgets/tenants.html',
    "<md-content ng-controller=\"AmhTenantsCtrl as ctrl\" ng-init=\"ctrl.setDataQuery('{id, domain, level}')\" ng-init=\"ctrl.queryParameter.removeSorter('id')\" ng-init=\"ctrl.queryParameter.setOrder('level', 'd')\" mb-infinate-scroll=ctrl.loadNextPage() mb-preloading=ctrl.loading class=md-padding layout=column layout-align=\"start center\" layout-padding layout-margin flex style=\"background-color: transparent\"> <md-input-container class=\"md-icon-float md-block\"> <label translate=\"\">Search</label> <wb-icon>search</wb-icon> <input ng-model=searchKey ng-keyup=\"$event.keyCode === 13 && ctrl.search(searchKey)\"> </md-input-container>  <div layout=column layout-gt-sm=row layout-align=\"space-around center\" layout-wrap> <md-card ng-if=ctrl.items.length ng-repeat=\"tenant in ctrl.items track by tenant.id\" flex-xs flex-gt-xs=none layout=column> <md-card-content> <div class=thumbnail-container> <div class=thumbnail> <iframe src={{ctrl.secureUrl(tenant.domain)}} frameborder=0 onload=\"this.style.opacity = 1\"> </iframe> </div> </div> </md-card-content>  <md-card-actions layout=row layout-padding layout-align=\"end center\"> <md-button flex title=http://{{::tenant.domain}} ng-href=http://{{::tenant.domain}}> <span translate>Visit the site now</span> </md-button> <lable translate>Rank: {{tenant.level|| 0}}</lable> </md-card-actions> </md-card> </div> </md-content> "
  );

}]);
